

## WIFI信息

### 查看已缓存的WIFI列表

```
adb root
adb shell cat data/misc/wifi/wpa_supplicant.conf
```



## 版本信息

### ROM版本

```
adb shell getprop ro.custom.build.version
```

### APP版本

```
adb shell dumpsys package 
```

例如固件版本：

```
adb shell dumpsys package com.zhaoguan.radarandroiddv1
```

## 蓝牙

### 查看蓝牙开关状态

```
adb shell settings get global bluetooth_on
```

